module gitlab.com/nharward/ghopac

go 1.23.3

require (
	github.com/google/go-github/v67 v67.0.0
	golang.org/x/oauth2 v0.0.0-20180821212333-d2e6202438be
)

require (
	github.com/golang/protobuf v1.3.2 // indirect
	github.com/google/go-querystring v1.1.0 // indirect
	golang.org/x/net v0.0.0-20210226172049-e18ecbb05110 // indirect
	google.golang.org/appengine v1.6.7 // indirect
)
